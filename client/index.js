import riot from 'riot'
import mixin from 'riot-redux-mixin'
import thunk from 'redux-thunk'
import { createStore, compose, combineReducers, applyMiddleware } from 'redux'

import search from './xdccsearch/search-duck'
import results from './xdccsearch/results-duck'
import transfers from './xdccsearch/transfers-duck'

import './xdccsearch/Xdccsearch.tag'
import './xdccsearch/DownloadsCountTab.tag'
import './xdccsearch/SearchBar.tag'
import './xdccsearch/ResultList.tag'
import './xdccsearch/ResultListItem.tag'
import './xdccsearch/ResultListCounts.tag'
import './xdccsearch/TransfersList.tag'
import './xdccsearch/TransfersListItem.tag'
import './xdccsearch/PollIndicator.tag'

import 'material-design-lite/material.min.css'
import 'material-design-lite/material.min.js'
import 'font-awesome/css/font-awesome.min.css'

const initialState = {}

const rootReducer = combineReducers({search, results, transfers})

let middleware = applyMiddleware(thunk)
if (process.env.NODE_ENV !== 'production' && window.devToolsExtension) {
  middleware = compose(middleware, window.devToolsExtension())
}

const store = createStore(
  rootReducer,
  initialState,
  middleware
)

document.addEventListener('DOMContentLoaded', () => {
  riot.mixin('redux', mixin(store))

  riot.mount('xdccsearch')
})
