import * as xhr from 'xhr-promise-redux'
import _ from 'lodash'

export const TRANSFER_STOP       = 'transfer/STOP'
export const TRANSFER_REMOVE     = 'transfer/REMOVE'
export const TRANSFER_ADD        = 'transfer/ADD'
export const TRANSFER_RECEIVE    = 'transfer/RECEIVE'
export const TRANSFER_FETCH      = 'transfer/FETCH'
export const TRANSFER_POLL_START = 'transfer/POLL_START'
export const TRANSFER_POLL_STOP  = 'transfer/POLL_STOP'

const initialState = {
  isPolling: false,
  active: {},
  transfers: {}
}

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case TRANSFER_POLL_START:
      return {...state, isPolling: true}

    case TRANSFER_POLL_STOP:
      return {...state, isPolling: false}

    case TRANSFER_ADD:
      return transferAdd(state, action.pack)

    case TRANSFER_STOP:
      return transferStop(state, action.pid)

    case TRANSFER_REMOVE:
      return transferRemove(state, action.pid)

    case TRANSFER_RECEIVE:
      return transferReceive(state, action.xfers)

    case TRANSFER_FETCH:
      return {...state, requesting: true}

    default: return state;
  }
}

const transferAdd = (state, pack) => {
  return Object.assign( {}, state, {
    transfers: {
      ...state.transfers,
      [pack.pid]: {
        state: 'NEW',
        pack
      }
    },
    active: { ...state.active }
  })
}

const transferStop = (state, pid) => {
  const transfers = {...state.transfers}
  const toBeStoppedXfer = {...state.transfers[pid]}
  toBeStoppedXfer.removing = true

  return Object.assign( {}, state, {
    transfers: {
      ...transfers,
      [pid]: toBeStoppedXfer
    }
  })
}

const transferRemove = (state, pid) => {
  const xfers = Object.assign( {}, state.transfers)
  const active = Object.assign( {}, state.active)

  delete xfers[pid]
  delete active[pid]

  return Object.assign( {}, state, {
    transfers: {
      ...xfers
    },
    active: { ...active }
  })
}

const transferReceive = (state, xfers) => {
  const stateXfers = _.cloneDeep(state)

  stateXfers.active = {}

  xfers && xfers.map(xfer => {
    const pid = xfer.pack.pid
    stateXfers.transfers[pid] = {
      ...stateXfers.transfers[pid],
      ...xfer
    }
    stateXfers.active[pid] = true
  })
  stateXfers.requesting = false
  return stateXfers
}



// Action Creators
export const add = pack => dispatch => {
  dispatch({ type: TRANSFER_ADD, pack })
  const url = '/api/xfers'

  return xhr.post(url, {
      json: pack,
      responseType: 'json'
    })
    .then(
      response => {
        const xfers = JSON.parse(response.body)
        dispatch(receive(xfers))
      }
    )
    .catch(response => {
      console.log(JSON.stringify(response));
    })
}

export const receive = xfers => {
  return { type: TRANSFER_RECEIVE, xfers }
}

export const removeTransfer = pid => {
  return { type: TRANSFER_REMOVE, pid }
}

const fetch = () => dispatch => {
  dispatch({ type: TRANSFER_FETCH })
  const url = '/api/xfers'
  return xhr.get(url)
    .then(
      response => {
        const xfers = JSON.parse(response.body)
        dispatch(receive(xfers))
      }
    )
    .catch(response => {
      console.log(JSON.stringify(response));
    })
}

export const pollXfers = () => (dispatch, getState) => {
  dispatch( {type: TRANSFER_POLL_START} )
  dispatch( fetch() )

  const intervalId = setInterval(() => {
    const state = getState()
    const isPolling = state.transfers.isPolling

    if (isPolling) {
      dispatch( fetch() )
    } else {
      clearInterval(intervalId)
    }
  }, 3000);
}

export const stopPoll = () => {
  return {type: TRANSFER_POLL_STOP}
}

export const stopTransfer = pid => (dispatch, getState) => {
  dispatch( {type: TRANSFER_STOP, pid} )
  const url = `/api/xfers/${pid}`

  return xhr.send(url, {method: 'DELETE'})
    .then(
      dispatch( fetch() )
    )
    .catch(response => {
      console.log(JSON.stringify(response));
    })

}
