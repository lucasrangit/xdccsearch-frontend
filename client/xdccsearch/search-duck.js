import * as xhr from 'xhr-promise-redux'
import { receiveResults, receivePageInfo, clearResults } from './results-duck'

const SEARCH_START   = 'search/START'
const SEARCH_ERROR   = 'search/ERROR'
const SEARCH_STOP    = 'search/STOP'


const initialState = {}

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case SEARCH_START: {
      return Object.assign( {}, state, {
          searching: true,
          error: false,
          query: action.query
        }
      )
    }

    case SEARCH_STOP: {
      return Object.assign( {}, state, {
          searching: false,
          error: false,
        }
      )
    }

    case SEARCH_ERROR: {
      return Object.assign( {}, state, {
          searching: false,
          error: true,
          errorMessage: action.error
        }
      )
    }

    default: return state;
  }
}

// Action Creators
export const query = (query, clear = false, page = 0) => dispatch => {
  dispatch(startSearch(query))
  const url = `/api/search?q=${query}&pn=${page}`
  return xhr.get(url)
    .then(function (response) {
      dispatch(stopSearch())
      return JSON.parse(response.body)
    })
    .then(response => {
      if (clear) {
        dispatch(clearResults(query))
      }
      dispatch(receivePageInfo(query, response))
      dispatch(receiveResults(query, response))
    })
    .catch(function(response) {
      const parsedResponseBody = JSON.parse(response.body)
      dispatch(errorSearch(response.statusCode, parsedResponseBody))
      console.log('Error! Response was:', response)
    })
}

export const startSearch = query => {
  return { type: SEARCH_START , query }
}

export const stopSearch = () => {
  return { type: SEARCH_STOP }
}


export const errorSearch = (statusCode, message) => {
  return { type: SEARCH_ERROR, error: message, statusCode }
}
