import test from 'tape'
import {reducerTest} from 'tape-redux'
import deepFreeze from 'deep-freeze'
import configureMockStore from 'redux-mock-store'
import nock from 'nock'
import thunk from 'redux-thunk'

import reducer from './transfers-duck'
import * as transfers from './transfers-duck'
import {TRANSFER_ADD, TRANSFER_FETCH, TRANSFER_RECEIVE, TRANSFER_REMOVE, TRANSFER_STOP} from './transfers-duck'


// https://www.snip2code.com/Snippet/931029/Testing-async-actions-on-redux-without-f
// const middlewares = [ thunk ]
// const mockStore = configureMockStore(middlewares)
// '/api/xfers'



test('transfers reducer TRANSFER_ADD add fresh new pack to initialState', t => {
  const stateBefore = {}
  const stateAfter = {
    active: { },
    transfers: {
      345: {
        state: 'NEW',
        pack: {
          pid: 345
        }
      }
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
    reducer(
      stateBefore,
      { type: TRANSFER_ADD, pack: {pid: 345}}
    ),
    stateAfter,
    ''
  )

  t.end()
})


test('transfers reducer TRANSFER_ADD add fresh new pack to already existing', t => {
  const stateBefore = {
    transfers: {
        345: {
          state: 'REQUEST',
          pack: {
            pid: 345
          }
        }
      }
    }

  const stateAfter = {
    active: { },
    transfers: {
      345: {
        state: 'REQUEST',
        pack: {
          pid: 345
        }
      },
      777: {
        state: 'NEW',
        pack: {
          pid: 777
        }
      }
    }
  }

  const action = { type: TRANSFER_ADD, pack: { pid: 777 } }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(reducer(stateBefore, action), stateAfter, '')

  t.end()
})


test('transfers reducer TRANSFER_RECEIVE updates xfer info, overwriting contained pack', t => {
  const stateBefore = {
    requesting: false,
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      }
    }
  }

  const stateAfter = {
    requesting: false,
    active: {
      345: true
    },
    transfers: {
      345: {
        pack: {
          pid: 345,
          cname: 'this pack replaces the previous'
        },
        state: 'NEW',
        localBotName: 'P_3x'
      }
    }
  }

  const action = {
    type: TRANSFER_RECEIVE,
    xfers: [
      {
        localBotName: 'P_3x',
        state: 'NEW',
        pack: {
          pid: 345,
          cname: 'this pack replaces the previous'
        }
      }
    ]
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(reducer(stateBefore, action), stateAfter, '')

  t.end()
})


test('transfers reducer TRANSFER_FETCH updates xfer info setting "requesting" true', t => {
  const stateBefore = {
    requesting: false,
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      }
    }
  }

  const stateAfter = {
    requesting: true,
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      }
    }
  }

  const action = { type: TRANSFER_FETCH }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(reducer(stateBefore, action), stateAfter, '')

  t.end()
})

test('transfers reducer TRANSFER_RECEIVE updates xfer info setting requesting false and active false', t => {
  const stateBefore = {
    requesting: true,
    active: {
      345: true
    },
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      }
    }
  }

  const stateAfter = {
    requesting: false,
    active: { },
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      }
    }
  }

  const action = {
    type: TRANSFER_RECEIVE,
    xfers: []
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(reducer(stateBefore, action), stateAfter, '')

  t.end()
})


test('transfers reducer TRANSFER_REMOVE removes the specified pack from transfers', t => {
  const stateBefore = {
    active: { },
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      },
      777: {
        pack: {
          pid: 777,
          name: 'moko'
        }
      }
    }
  }

  const stateAfter = {
    active: { },
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      }
    }
  }

  const action = {
    type: TRANSFER_REMOVE,
    pid: 777
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(reducer(stateBefore, action), stateAfter, '')

  t.end()
})


test('transfers reducer TRANSFER_STOP stops the specified transfer', t => {
  const stateBefore = {
    active: { },
    transfers: {
      345: {
        pack: {
          pid: 345,
          name: 'lala'
        }
      },
      777: {
        pack: {
          pid: 777,
          name: 'oink'
        }
      }
    }
  }

  const stateAfter = {
    active: { },
    transfers: {
      345: {
        removing: true,
        pack: {
          pid: 345,
          name: 'lala'
        }
      },
      777: {
        pack: {
          pid: 777,
          name: 'oink'
        }
      }
    }
  }

  const action = {
    type: TRANSFER_STOP,
    pid: 345
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(reducer(stateBefore, action), stateAfter, '')

  t.end()
})
