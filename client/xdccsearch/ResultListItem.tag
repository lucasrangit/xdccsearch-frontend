
<ResultListItem>

  <!-- <div style="display: inline" class={selected: selected, new: isNew, old: !isNew}> -->
  <div class="line {new: isNew} {isActive ? 'mdl-color-text--accent' : 'mdl-color-text--primary-dark'}">
    <h5 class="mdl-typography--headline pack-name">
      <i if={isNew} class="fa fa-circle-o-notch mdl-color-text--grey"></i> 
      <a href="#" onclick={searchForName} title="search for '{pack.name}' only">{pack.name} </a>
    </h5>

    <div class="mdl-grid pack-info {selected: transfer}">

      <div class="mdl-cell mdl-cell--9-col">
        <i class="fa fa-size"></i> {pack.szf} 
        : <i class="fa fa-cloud"></i> {pack.nname} 
        : <i class="fa fa-user"></i> {pack.uname}
        {": " + transfer.state}
      </div>
      <div class="mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet">
        <a class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent"
            href="#"
            disabled={ isActive }
            onClick={ handleClick }>
          <i class="fa fa-download"></i> download
        </a>
      </div>
    </div>
  </div>

  <style scoped>
    .new > .pack-info {
      font-weight: 400;
    }

    .line > h5 {
      margin-top: 0;
      margin-bottom: 0;
      white-space: nowrap;
      overflow: scroll;
    }

    .pack-name > a {
      color: inherit;
    }

    .pack-name:hover > .fa-search {
      visibility: visible;
    }

    .pack-name > .fa-search {
      visibility: hidden;
      margin-left: -2rem;
    }

    .pack-info {
      line-height: 36px;
      /* color: grey; */
      font-weight: 100;
    }

    .pack-info.selected {
      font-weight: 600;
    }

    .line {
      border-bottom: #eee solid 1px;
      padding: 16px 16px 0;
    }

    .line:hover {
      background-color: #fafafa;
    }

    .line > .mdl-grid {
      padding: 0;
    }
  </style>

  <script type="babel">
    this.pack = this.opts.pack
    this.handleClick = this.opts.handle_click
    this.transfer = this.opts.transfer
    this.isNew = this.opts.is_new
    this.isActive = this.opts.is_active
    this.searchForName = this.opts.handle_search_for


    this.on('update', () => {
      this.isNew = this.opts.is_new
      this.transfer = this.opts.transfer
      this.isActive = this.opts.is_active
    })
  </script>
</ResultListItem>
