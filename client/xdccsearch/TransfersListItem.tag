<TransfersListItem>
  <div class="mdl-card mdl-shadow--4dp item">
    <div class="mdl-card__title">
      <h4 class="mdl-card__title-text">
        {transfer.pack.name}
      </h4>
    </div>

    <div class="mdl-card__supporting-text">
      <div id="{transfer.pack.pid}">
        <i class="fa {getIconForState(transfer.state)} state-icon fa-2x"></i>
        <span class="mdl-typography--title">
          <i class="fa {active ? 'fa-heartbeat' : 'fa-heart-o'}" ></i>{ }
          <span class="">
            {sizeFormatter(transfer.xferBytes)} of {sizeFormatter(transfer.xferBytesTotal)} ({percentDone(transfer)}%)
          </span>
          : <i class="fa fa-user"></i> {transfer.pack.uname}

        </span>
      </div>
      <div class="mdl-tooltip" for="{transfer.pack.pid}">{transfer.state}</div>      
    </div>

    <div class="mdl-card__actions mdl-card--border">
      <a class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdl-button--raised"
        onclick={buttonClickHandler}>
        <i class="fa fa-remove"></i> {buttonText}</a>
    </div>

  </div>



  <style scoped>
    a.mdl-button > i.fa {
      font-size: 16px;
    }

    .state-icon {
      margin-right: 30px;
    }

    .item.active {
      color: #0a0;
    }

    .item.active.removing {
      color: #f44;
    }

    h5 {
      margin-top: 0;
      margin-bottom: 0;
      white-space: nowrap;
      text-overflow: ellipsis;
    }

    .mdl-card__supporting-text {
      padding-top: 0;
    }

    .mdl-card.item {
      width: 90%;
      margin: 2em auto;
      min-height: 4em;
    }

  </style>

  <script type="babel">
    import { sizeFormatter } from './utils.js'

    this.active = this.opts.active
    this.transfer = this.opts.transfer
    this.searchForName = this.opts.handle_search_for

    const stopTransferHandler = this.opts.handle_stop_transfer
    const removeFromListHandler = this.opts.handle_remove_from_list

    this.sizeFormatter = sizeFormatter
    this.done = false

    this.buttonText = 'cancel'
    this.buttonClickHandler = e => {}

    this.on('update', () => {
      this.active = this.opts.active
      this.transfer = this.opts.transfer
      // debugger
      this.done = this.transfer ? this.isDone(this.transfer.state) : true
      updateButton()
    })

    const updateButton = () => {
      if (!this.done){
        this.buttonText = this.active ? "stopTransferHandler" : "removeFromListHandler"
        this.buttonClickHandler = this.active ? stopTransferHandler : removeFromListHandler
      } else {
        this.buttonText = this.active ? "stopAndRemoveHandler" : "removeFromListHandler"
        this.buttonClickHandler = this.active ? stopAndRemoveHandler : removeFromListHandler
      }
    }

    const stopAndRemoveHandler = e => {
      console.log('stopAndRemoveHandler', e);
      stopTransferHandler(e)
      removeFromListHandler(e)
    }

    this.isDone = xferState => {
      switch (xferState) {
        case 'DONE': 
        case 'ERROR': 
        case 'TRANSFER_ERROR': return true;
      }
      return false
    }

    this.getIconForState = xferState => {
      switch (xferState) {
        case 'NEW': return 'fa-circle-o-notch';
        case 'REQUEST': return 'fa-exchange';
        case 'QUEUE': return 'fa-bars fa-rotate-90';
        case 'INCOMING': return 'fa-exchange mdl-color-text--brown-100';
        case 'TRANSFER': return 'fa-refresh fa-spin';
        case 'DONE': return 'fa-check fa-4x mdl-color-text--green';
        case 'ERROR': return 'fa-frown-o fa-4x mdl-color-text--red';
        case 'TRANSFER_ERROR': return 'fa-frown-o fa-4x mdl-color-text--red';
      }
    }

    this.percentDone = xfer => {
      return (xfer.xferBytesTotal > 0) ? (xfer.xferBytes / xfer.xferBytesTotal * 100).toFixed(2) : 0
    }

  </script>

</TransfersListItem>