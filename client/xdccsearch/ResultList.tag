
<ResultList>

  <section
    if={current.length == '' && queries.length == 0}
    class="empty-list">
    <h2 class="mdl-typography--display-2-color-contrast mdl-typography--text-center">enter search</h2>
  </section>

  <section
    if={current.length == ''  && queries.length > 0}
    class="empty-list">
    <h2 class="mdl-typography--display-2-color-contrast mdl-typography--text-center">enter or select search</h2>
  </section>

  <section
    if={packs.length == 0 && current.length != ''}
    class="empty-list">
    <h2 class="mdl-typography--display-2-color-contrast mdl-typography--text-center">nothing found for {current}</h2>
  </section>

  <section if={packs.length > 0}>
    <h2 class="mdl-typography--display-2-color-contrast">
      <i class="fa fa-search"></i> search results
    </h2>

    <div class="mdl-typography--headline">
      sort by
      <a href="#" onclick={ sortByHandler.bind(this, 'name') } class={orderby-selected: orderBy.field == 'name'}>name</a>,
      <a href="#" onclick={ sortByHandler.bind(this, 'sz') } class={orderby-selected: orderBy.field == 'sz'}>size</a>,
      <a href="#" onclick={ sortByHandler.bind(this, 'nname') } class={orderby-selected: orderBy.field == 'nname'}>network</a>
    </div>

    <div if="{packs.length > 0}" class="mdl-card mdl-shadow--4dp">
      <ResultListItem each={ pack in packs }
                      pack={ pack }
                      handle_click={ parent.selectPack }
                      transfer={ parent.getTransfer(pack.pid) }
                      is_new={ parent.isNew(pack.pid) }
                      handle_search_for={ parent.searchForName }
                      is_active={ parent.isActive(pack.pid) }>
      </ResultListItem>
    </div>

    <div if={ hasMorePages } class="loadMoreResults">
      <a onclick={ loadMore } class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdl-button--raised" >
      <i class="fa fa-refresh {fa-spin: searching}"></i> Load More</a> 
    </div>
  </section>

  <style scoped>
    .orderby-selected {
      text-decoration: underline;
    }

    section > .mdl-typography--headline {
      margin: 0 5%;
    }

    section > h2 {
      text-align: center;
    }

    section.empty-list {
      min-height: 60vh;
      display: flex;
    }

    section.empty-list > h2 {
      margin: auto;
    }

    ul {
      margin: 0;
      padding: 0;
      list-style-type: none;
    }

    ResultListItem:last-of-type > .item {
      margin-bottom: 0;
    }

    .loadMoreResults {
      display: flex;
      margin: 5em 0;
    }

    .loadMoreResults > a.mdl-button {
      margin: auto;
      width: 15em;
    }

    .mdl-card { 
      width: 90%;
      margin: 2em auto;
      min-height: 4em;
    }

  </style>

  <script type="babel">
    this.mixin('redux')

    import { createSelector } from 'reselect'
    import { getCurrentQuery, getCurrentResults, getCurrentPacks, getNewPackIds, sortBy, getQueries } from './results-duck'
    import { add } from './transfers-duck'
    import { query } from './search-duck'

    const getTransfers = state => state.transfers.transfers
    const getActiveTransfers = state => state.transfers.active
    const getSortBy = state => state.results.sortBy
    const getSearching = state => state.search.searching

    const selector = createSelector(
      [getCurrentQuery, getCurrentResults, getCurrentPacks, getTransfers, getNewPackIds, getSortBy, getActiveTransfers, getQueries, getSearching],
      (current, results, packs, transfers, newPackIds, sortBy, active, queries, searching) => {
        return {
          packs,
          hasMorePages: results.hasMorePages,
          currentPage: results.currentPage,
          current,
          transfers,
          newPackIds,
          orderBy: sortBy,
          active,
          queries: Object.keys(queries),
          searching
        }
      }
    )

    this.subscribe( selector )

    this.getTransfer = pid => this.transfers[pid]
    
    this.isActive = pid => this.active[pid]

    this.isNew = pid => this.newPackIds[pid] !== undefined

    this.loadMore = event => {
      if (this.hasMorePages) {
        this.dispatch(query(this.current, false, this.currentPage + 1))
      }
    }

    this.selectPack = event => this.dispatch( add(event.item.pack) )

    this.sortByHandler = field => this.dispatch( sortBy(field) )

    this.searchForName = e => {
      const pack = e.item.pack
      this.dispatch(query(pack.name))
    }
  </script>
</ResultList>
