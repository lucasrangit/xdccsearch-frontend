import test from 'tape'
import deepFreeze from 'deep-freeze'

import resultsReducer from './results-duck'
import * as results from './results-duck'

test('results reducer RESULTS_RECEIVE populates the results subtree on initial state', (t) => {
  const resultsReceived = {
    results: [
      { pid: 1 },
      { pid: 2 }
    ]
  }
  const stateBefore = results.initialState
  const stateAfter = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    refs: {
      1: 1,
      2: 1,
    },
    new: {
      1: true,
      2: true
    },
    sortBy: {field: '', direction: 'asc'}
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receiveResults('ABC', resultsReceived)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_RECEIVE receive on prepopulated state', (t) => {
  const resultsReceived = {
    results: [
      { pid: 6 },
      { pid: 7 }
    ]
  }

  const stateBefore = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      }
    },
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    current: 'ABC',
    refs: {
      1: 1,
      2: 1,
    },
    new: {
      1: true,
      2: true
    }
  }

  const stateAfter = {
    queries: {
      'ABC': {
        packIds: [ 1, 2 ]
      },
      'ANO': {
        packIds: [ 6, 7 ]
      }
    },
    packs: {
      1: { pid: 1 },
      2: { pid: 2 },
      6: { pid: 6 },
      7: { pid: 7 }
    },
    current: 'ANO',
    refs: {
      1: 1,
      2: 1,
      6: 1,
      7: 1
    },
    new: {
      6: true,
      7: true
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receiveResults('ANO', resultsReceived)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_RECEIVE receive on prepopulated state with same query: append to results list', (t) => {
  const resultsReceived = {
    results: [
      { pid: 6 },
      { pid: 7 }
    ]
  }

  const stateBefore = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    refs: {
      1: 1,
      2: 1,
    },
    new: {
      1: true,
      2: true
    }
  }

  const stateAfter = {
    queries: {
      'ABC': {
        packIds: [1, 2, 6, 7]
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 },
      6: { pid: 6 },
      7: { pid: 7 }
    },
    refs: {
      1: 1,
      2: 1,
      6: 1,
      7: 1
    },
    new: {
      6: true,
      7: true
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receiveResults('ABC', resultsReceived)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_RECEIVE receive on prepopulated state with same query and overlapping packs', (t) => {
  const resultsReceived = {
    results: [
      { pid: 2 },
      { pid: 3 }
    ]
  }

  const stateBefore = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    refs: {
      1: 1,
      2: 1,
    },
    new: {
      1: true,
      2: true
    }
  }

  const stateAfter = {
    queries: {
      'ABC': {
        packIds: [1, 2, 3]
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 },
      3: { pid: 3 },
    },
    refs: {
      1: 1,
      2: 2,
      3: 1
    },
    new: {
      2: true,
      3: true,
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receiveResults('ABC', resultsReceived)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_RECEIVE populates the results subtree leaving previous page information in place', (t) => {
  const resultsReceived = {
    results: [
      { pid: 1 },
      { pid: 2 }
    ]
  }
  const stateBefore = {
    queries: {'ABC': {
      currentPage: 0,
      hasMorePages: true
    }},
    current: '',
    new: {}
  }

  const stateAfter = {
    queries: {
      'ABC': {
        packIds: [1, 2],
        currentPage: 0,
        hasMorePages: true
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    refs: {
      1: 1,
      2: 1
    },
    new: {
      1: true,
      2: true
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receiveResults('ABC', resultsReceived)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_CLEAR clears the query results subtree', (t) => {
  const stateBefore = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    refs: {
      1: 1,
      2: 1,
    },
    new: {
      1: true,
      2: true
    }
  }

  const stateAfter = {
    queries: {},
    current: '',
    packs: {},
    refs: {},
    new: {
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.clearResults('ABC')),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_CLEAR clears the query results subtree - leaving others untouched', (t) => {
  const stateBefore = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      },
      'JUN': {
        packIds: [6, 7]
      }
    },
    current: 'JUN',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 },
      6: { pid: 6 },
      7: { pid: 7 }
    },
    refs: {
      1: 1,
      2: 1,
      6: 1,
      7: 1,
    },
    new: {}
  }

  const stateAfter = {
    queries: {
      'JUN': {
        packIds: [6, 7]
      }
    },
    current: 'JUN',
    packs: {
      6: { pid: 6 },
      7: { pid: 7 }
    },
    refs: {
      6: 1,
      7: 1,
    },
    new: {}
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.clearResults('ABC')),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_CLEAR clears the query results subtree - clearing decrementing refcount', (t) => {
  const stateBefore = {
    queries: {
      'ABC': {
        packIds: [1, 2]
      },
      'JUN': {
        packIds: [2, 3]
      },
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 },
      3: { pid: 3 },
    },
    refs: {
      1: 1,
      2: 2,
      3: 1,
    },
    new: {}
  }

  const stateAfter = {
    queries: {
      'JUN': {
        packIds: [2, 3]
      }
    },
    current: '',
    packs: {
      2: { pid: 2 },
      3: { pid: 3 }
    },
    refs: {
      2: 1,
      3: 1
    },
    new: {}
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.clearResults('ABC')),
      stateAfter,
      ''
  )

  t.end()
})


test('results reducer RECEIVE_PAGE_INFO append currentPage and hasMorePages', (t) => {
  const response = { pn: 0, pc: 2 }
  const stateBefore = results.initialState
  const stateAfter = {
    queries: {
      'ABC': {
        currentPage: 0,
        hasMorePages: true
      }
    },
    current: '',
    packs: {},
    new: {},
    refs: {},
    sortBy: {field: '', direction: 'asc'}
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receivePageInfo('ABC', response)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RECEIVE_PAGE_INFO append pageNum to already existing contents node', (t) => {
  const response = { pn: 0, pc: 2 }
  const stateBefore = {
    queries: {'ABC': {
      packIds: [],
    }},
    current: '',
    packs: {}
  }

  const stateAfter = {
    queries: {'ABC': {
      packIds: [],
      currentPage: 0,
      hasMorePages: true
    }},
    current: '',
    packs: {}
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receivePageInfo('ABC', response)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RECEIVE_PAGE_INFO set pageNum and hasMorePages to already existing contents node', (t) => {
  const response = { pn: 2, pc: 4 }
  const stateBefore = {
    queries: {'ABC': {
      packIds: [],
      currentPage: 1,
      hasMorePages: true
    }},
    current: '',
    packs: {}
  }

  const stateAfter = {
    queries: {ABC: {
      packIds: [],
      currentPage: 2,
      hasMorePages: true
    }},
    current: '',
    packs: {}
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receivePageInfo('ABC', response)),
      stateAfter,
      ''
  )

  t.end()
})

test('results reducer RESULTS_RECEIVE set the query results, keeping previous paging information in place', (t) => {
  const response = {
    pn: 4,
    pc: 6,
    results: [
      { pid: 1 },
      { pid: 2 }
    ]
  }

  const stateBefore = {
    queries: {
      'ABC': {
        hasMorePages: true,
        currentPage: 3
      }
    },
    current: 'ABC',
    packs: {}
  }

  const stateAfter = {
    queries: {
      'ABC': {
        packIds: [1, 2],
        hasMorePages: true,
        currentPage: 3
      }
    },
    current: 'ABC',
    packs: {
      1: { pid: 1 },
      2: { pid: 2 }
    },
    refs: {
      1: 1,
      2: 1,
    },
    new: {
      1: true,
      2: true
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.receiveResults('ABC', response)),
      stateAfter,
      ''
  )

  t.end()
})


test('results reducer RESULTS_SELECT_QUERY set the query results', (t) => {
  const stateBefore = {
    queries: {
      'ABC': {
        hasMorePages: true,
        currentPage: 3
      },
      'XXX': {
        hasMorePages: true,
        currentPage: 3
      }
    },
    current: 'ABC'
  }

  const stateAfter = {
    queries: {
      'ABC': {
        hasMorePages: true,
        currentPage: 3
      },
      'XXX': {
        hasMorePages: true,
        currentPage: 3
      }
    },
    current: 'XXX'
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.selectQuery('XXX')),
      stateAfter,
      ''
  )

  t.end()
})


test('results reducer RESULTS_SORT_BY sets sort_by field', (t) => {

  const stateBefore = results.initialState
  const stateAfter = {
    queries: {},
    current: '',
    packs: {},
    new: {},
    refs: {},
    sortBy: {
      field: 'pid',
      direction: 'asc'
    }
  }

  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      resultsReducer(stateBefore, results.sortBy('pid', 'asc')),
      stateAfter,
      ''
  )

  t.end()
})
