import test from 'tape'
import {reducerTest} from 'tape-redux'
import deepFreeze from 'deep-freeze'

import reducer from './search-duck'
import * as search from './search-duck'

test('search reducer SEARCH_START', (t) => {
  const stateBefore = {}
  const stateAfter = {
		searching: true,
		error: false,
    query: 'bla'
	}
  deepFreeze(stateBefore)
  deepFreeze(stateAfter)

  t.deepEqual(
      reducer(stateBefore, search.startSearch('bla')),
      stateAfter,
      ''
  )

  t.end()
})


test('search reducer SEARCH_START clearing previous error and sets query', reducerTest(
	reducer,
	{error: true},
	search.startSearch.bind(null, 'something'),
	{searching: true, error: false, query: 'something'}
))

test('search reducer SEARCH_STOP clears previous error and query', reducerTest(
	reducer,
	{error: true},
	search.stopSearch,
	{searching: false, error: false}
))

test('search reducer SEARCH_ERROR clears searching flag', reducerTest(
	reducer,
	{searching: true, error: false},
	search.errorSearch.bind(null, 400, 'some error'),
	{searching: false, error: true, errorMessage: 'some error'}
))
