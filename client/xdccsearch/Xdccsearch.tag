<Xdccsearch>

  <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header
              mdl-layout--fixed-tabs">
     <header class="mdl-layout__header">
      <div class="mdl-layout__header-row">
        <span class="mdl-layout-title" for="search">XDCC Search</span>
      </div>

      <!-- Tabs -->
      <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
        <a href="#search" class="mdl-layout__tab is-active">Search</a>
        <a href="#downloads" class="mdl-layout__tab">
          <DownloadsCountTab></DownloadsCountTab>
        </a>
      </div>
    </header>


    <main class="mdl-layout__content mdl-color--pink-50">

      <section class="mdl-layout__tab-panel is-active" id="search">
        <div class="page-content">

          <div class="mdl-grid mdl-color--indigo-100 search">
            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
              <SearchBar></SearchBar>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
              <ResultListCounts></ResultListCounts>
            </div>
          </div>

          <ResultList></ResultList>    
        </div>
      </section>

      <section class="mdl-layout__tab-panel" id="downloads">
        <div class="page-content">
          <PollIndicator></PollIndicator>
          <TransfersList></TransfersList>
        </div>
      </section>

    </main>
  </div>

  <style scoped>
    .search {
      padding: 2em;
    }
  </style>

  <style>
    a {
      text-decoration: none;
    }
  </style>

</Xdccsearch>
