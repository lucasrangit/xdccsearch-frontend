<SearchBar>
  <form name="searchForm" onsubmit={ searchQuery } class="mdl-color--indigo-50 search">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {is-dirty: dirty}">
      <input
        class="mdl-textfield__input"
        type="text"
        name="searchInput"
        autofocus="autofocus"
        required="required"
        id="searchInput"
         />
      <label
        class="mdl-textfield__label"
        for="searchInput">search for...</label>
    </div>
    <i class="fa fa-refresh fa-spin fa-2x" if={search.searching}></i>  
  </form>

  <div if={ search.error }>
    query returned statusCode = { search.statusCode }
  </div>

  <style type="text/css" media="screen" scoped>
    .search {
      padding: 1em;
    }

    .mdl-textfield {
      width: 90%;
    }
  </style>

  <script type="babel">
    this.mixin('redux')
    import { query } from './search-duck'
    import { createSelector } from 'reselect'

    const getCurrentQuery = state => state.results.current
    const getSearch = state => state.search

    this.searchForm.setAttribute( "autocomplete", "off" )
    this.searchInput.setAttribute( "autocomplete", "off" )



    const selector = createSelector(
      [getCurrentQuery, getSearch],
      (currentSearchQuery, search) => {
        return {
          search,
          currentSearch: currentSearchQuery
        }
      }
    )

    // this.dispatch( query("test", true) )

    this.subscribe(selector)

    this.on('update', state => {
      if (!this.search.searching)
        this.searchInput.value = this.currentSearch
      if (this.searchInput.value != '')
        this.dirty = true
    })

    this.searchQuery = (event) => {
      this.dispatch( query(this.searchInput.value, true) )
    }
  </script>
</SearchBar>
