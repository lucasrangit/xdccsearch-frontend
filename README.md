# XDCC Search Frontend

This is a [RiotJS](http://riotjs.com/) based frontend of the XDCC Search tool serving as backend.


## Getting started

The following commands are to be run from the command line unless otherwise specified. 

### prerequisites

npm. Install this using, for example, brew:

`brew install npm`

### install dependencies
... to build and start developing.

`npm install`

this will create the `node_modules` directory where all the project dependencies are kept, locally.


### run the service 

start the development server. it will take care of watching code changes, compiling and serving the frontend while supplying mock data:

`npm run start` 


### develop 

#### build, after you do your changes (build manually):

`npm run build` 

#### production ready build. the resulting `bundle.js` will be relatively small. :

`npm run build:prod`

#### automatic builds, whenever you do your changes. this will be a foreground process:

`npm run build:watch`


### run tests

`npm test` - run the unit tests

### deployment

copy all content from the `public` directory wherever you need or want it to be. preferably somewhere with a web server of some sort.


enjoy.